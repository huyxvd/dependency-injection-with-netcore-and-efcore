﻿using Microsoft.EntityFrameworkCore;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

// Register with Ioc container
builder.Services.AddControllers();

string conn = "Data Source=.;Database=ContextWithOption;Trusted_Connection=True;TrustServerCertificate=True";

// Đăng ký sử dụng EF
builder.Services.AddDbContext<AppDbContext>(x => x.UseSqlServer(conn));

WebApplication app = builder.Build();

app.MapControllers();

app.Run();
